package com.employee;


import java.util.Date;

public class EmployeeInfo {
	private String name,jobTitle ;
	private char sex;
	private Date birthday;
	private OrganizationInfo org;
	
	
	public EmployeeInfo (String name, char sex, String jobTitle, Date birthday, OrganizationInfo org) {
		this.name = name;
		this.sex = sex;
		this.jobTitle = jobTitle;
		this.birthday = birthday;
		this.org = org;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public char getSex() {
		return sex;
	}

	public void setSex(char sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public OrganizationInfo getOrg() {
		return org;
	}

	public void setOrg(OrganizationInfo org) {
		this.org = org;
	}


	
	public void printEmp ()  {
		
		System.out.println("Emp Name: " +name + "\nSex: " +sex + "\nJob Title: " +jobTitle + "\nBirthday: " + birthday + "\nOrganization: " + org.orgName);
	}






}
