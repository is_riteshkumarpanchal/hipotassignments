package com.employee;


public class OrganizationInfo {

	public String orgName;
	
	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public OrganizationInfo(String orgName) {
		this.orgName = orgName;
	}
	
	
	
}
