package com.employee;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmployeeTracker/* implements Comparable<EmployeeInfo>*/{

	public static void main(String[] args) throws ParseException, NumberFormatException, IOException {
		
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		/*OrganizationInfo org1 = new OrganizationInfo("Google");
		OrganizationInfo org2 = new OrganizationInfo("Microsoft");
		
		EmployeeInfo emp1 = new EmployeeInfo("Ritesh Panchal", 'M', "SE",sdf.parse("12/03/1991") , org2);
		EmployeeInfo emp2 = new EmployeeInfo("Rahul Patel", 'M', "SSE",sdf.parse("11/01/1988") , org1);
		EmployeeInfo emp3 = new EmployeeInfo("Sweta Tiwari", 'F', "QA Lead",sdf.parse("11/01/1991") , org1);
		EmployeeInfo emp4 = new EmployeeInfo("Roshan Lal", 'M', "HR",sdf.parse("11/01/1991") , org1);
		EmployeeInfo emp5 = new EmployeeInfo("Neena Gupta", 'F', "Sr. HR",sdf.parse("11/01/1991") , org1);
		EmployeeInfo emp6 = new EmployeeInfo("Ashok Singhal", 'M', "Admin",sdf.parse("11/01/1991") , org1);
		EmployeeInfo emp7 = new EmployeeInfo("Rima Katariya", 'F', "Manger",sdf.parse("11/01/1991") , org1);
		EmployeeInfo emp8 = new EmployeeInfo("Ritesh Panchal", 'M', "Sales Manager",sdf.parse("11/01/1991") , org1);
		
		emp1.printEmp();*/
		
		int choice;
		List<EmployeeInfo> empList = new ArrayList<EmployeeInfo>();
		do {
			System.out.println("*****Menu******");
			System.out.println("Press<1> to add employee");
			System.out.println("Press<2> to know the count of employees in the organization");
			System.out.println("Press<3> to exit");
			choice = Integer.parseInt(br.readLine());
			if(choice == 1) {
				System.out.println("Enter name ");
				String empName = br.readLine();
				System.out.println("Enter Sex [M] -Male, [F] - Female");
				char empSex = br.readLine().charAt(0);
				System.out.println("Enter Job Title ");
				String empJobTitle = br.readLine();
				System.out.println("Enter DOB in dd/mm/yyyy format");
				Date empDOB = sdf.parse(br.readLine());
				System.out.println(" Please select the organization from below");
				System.out.println(" Press <<G>> for Google");
				System.out.println(" Press <<M>> for Microsoft");
				
				String empOrg = br.readLine();
				if(empOrg.equalsIgnoreCase("G"))
					empOrg = "Google";
				else if(empOrg.equalsIgnoreCase("M"))
					empOrg = "Microsoft";
				OrganizationInfo newEmpOrg = new OrganizationInfo(empOrg);
				EmployeeInfo newEmployee = new EmployeeInfo(empName, empSex, empJobTitle, empDOB, newEmpOrg);
				System.out.println(empName+ empSex+  empJobTitle+ empDOB + newEmpOrg.orgName);//testing to check input is read as it is or not
				
				//adding a new Employee
				addNewEmployee(empList, newEmployee);
			}
			else if(choice == 2) {
				getOrgCount(empList);
			}
		}while(choice!= 3);
	}
	
	public static void addNewEmployee(List<EmployeeInfo> empList, EmployeeInfo emp) {
		empList.add(emp);
		if(empList.size() > 1) {
			for (int i = 0; i < empList.size() - 1; i++) {
					EmployeeInfo employeeInfo = empList.get(i);
					if(employeeInfo.getName().equalsIgnoreCase(emp.getName()) && employeeInfo.getSex() == (emp.getSex()) && employeeInfo.getJobTitle().equalsIgnoreCase(emp.getJobTitle()) && 
							employeeInfo.getBirthday().equals(emp.getBirthday()) && employeeInfo.getOrg().getOrgName().equals(emp.getOrg().getOrgName())) {
						System.out.println("Your details already exists");
						empList.remove(emp);
						break;
					}	
				}
			System.out.println("Lis of Employees :");
			for (EmployeeInfo employeeInfo : empList) {
				System.out.println("Name :"+employeeInfo.getName());
				System.out.println("Sex :"+employeeInfo.getSex());
				System.out.println("Job Title :"+employeeInfo.getJobTitle());
				System.out.println("DOB :"+employeeInfo.getBirthday());
				System.out.println("Organization Name :"+ employeeInfo.getOrg().getOrgName());
			}
		}
	}

	/*@Override
	public int compareTo(EmployeeInfo employee) {
		
		return 0;
	}*/
	public static void getOrgCount(List<EmployeeInfo> empList) {
		
		int countGoogle = 0;
		int countMS = 0;
		for (EmployeeInfo employeeInfo : empList) {
			String orgName = employeeInfo.getOrg().getOrgName();
			if (orgName.equalsIgnoreCase("Google"))
					countGoogle = countGoogle +1;
			else if (orgName.equalsIgnoreCase("Microsoft"))
				countMS = countMS +1;
				
		}
		System.out.println("Number of Employees in Google: " + countGoogle);
		System.out.println("Number of Employees in Microsoft: " + countMS);
		
	}
	
}
