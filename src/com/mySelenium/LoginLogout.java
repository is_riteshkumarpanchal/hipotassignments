package com.mySelenium;


import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginLogout {

	public static void main(String[] args) throws IOException, InterruptedException {
		
		WebDriver driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver", "D:\\300 Tools & Softwares\\chromedriver.exe");
		
		driver.get("http://newtours.demoaut.com/");
		
		FileReader reader=new FileReader("D:/Ritesh Learnings/HiPot_QA2QE/JavaLearning/src/com/mySelenium/input.properties");
		Properties prop = new Properties();	
		prop.load(reader);
		
		WebDriverWait wait = new WebDriverWait(driver,30);
		
		driver.findElement(By.xpath("//*[@name='userName']")).sendKeys(prop.getProperty("username"));
		driver.findElement(By.xpath("//*[@name='password']")).sendKeys(prop.getProperty("password"));
		
		
		driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
		driver.findElement(By.xpath("//*[@name='login']")).click(); //Login
		
		
			try {
				driver.findElement(By.xpath("//*[contains(text(),'SIGN-OFF')]")).isDisplayed();
				System.out.println("User Logged In");
					
			}
			catch (Exception e1) {
				System.out.println("User not Logged In");
			}	
			
			driver.findElement(By.xpath("//*[@name='findFlights']")).click();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//*[@name='reserveFlights']")).click();
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//*[@name='passFirst0']")).sendKeys(prop.getProperty("firstname"));
			driver.findElement(By.xpath("//*[@name='passLast0']")).sendKeys(prop.getProperty("lastname"));
			driver.findElement(By.xpath("//*[@name='creditnumber']")).sendKeys(prop.getProperty("cardnumber"));
			driver.findElement(By.xpath("//*[@name='buyFlights']")).click(); //Flight confirmation Page
			
			
			driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
			driver.findElement(By.xpath("//table//td[3]/a/img")).click(); //Log out
			//driver.findElement(By.xpath("//*[contains(text(),'SIGN-OFF')]")).click(); //Log out
			System.out.println("Logged Out");
					
		driver.close();
		}
		
		
	}




