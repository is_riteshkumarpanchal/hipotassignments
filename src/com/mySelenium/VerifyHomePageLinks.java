package com.mySelenium;

import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class VerifyHomePageLinks {
	@Test
	public static void main(String[] args) {
		
		WebDriver driver = new ChromeDriver();
		System.setProperty("webdriver.chrome.driver", "D:\\300 Tools & Softwares\\chromedriver.exe");
		
		driver.get("http://newtours.demoaut.com/"); //Open URL
		driver.manage().window().maximize();
		
		//Print Left Menu Links
		System.out.println("*********Left Menu*********");
		List<WebElement> leftMenuLinks = driver.findElements(By.xpath("//div/table/tbody/tr/td[1]/table//table//a"));
		for (int i=0; i <leftMenuLinks.size(); i++ ) {
			System.out.println(leftMenuLinks.get(i).getText());
		}
		
		//Print Top Menu Links
		System.out.println("\n*********Top Menu*********");
		List<WebElement> topMenuLinks = driver.findElements(By.xpath("//div/table//table//tr[2]/td/table//tr//a"));
		for (int i=0; i <topMenuLinks.size(); i++ ) {
			System.out.println(topMenuLinks.get(i).getText());
		}
		
		//Print Sections
		System.out.println("\n*********Sections*********");
		List<WebElement> sectionLinks = driver.findElements(By.xpath("//table//tr[4]/td//table//img[@alt]"));
		for (int i=0; i <sectionLinks.size(); i++ ) {
			System.out.println(sectionLinks.get(i).getAttribute("alt"));
		}
				
		
		//Left Section Locators
		List<WebElement> homeLink = driver.findElements(By.xpath("//td//a[contains(text(),'Home')]"));
		List<WebElement> flightsLink = driver.findElements(By.xpath("//td//a[contains(text(),'Flights')]"));
		List<WebElement> hotelsLink = driver.findElements(By.xpath("//td//a[contains(text(),'Hotels1')]")); //Wrong xpath entered to verify
		
		WebElement carRentalsLink = driver.findElement(By.xpath("//td//a[contains(text(),'Car Rentals')]"));
		WebElement cruisesLink = driver.findElement(By.xpath("//td//a[contains(text(),'Cruises')]"));
		WebElement destinationsLink = driver.findElement(By.xpath("//td//a[contains(text(),'Destinations')]"));
		WebElement vacationsLink = driver.findElement(By.xpath("//td//a[contains(text(),'Vacations')]"));
		
		//Top Menu Locators
		WebElement singOnLink = driver.findElement(By.xpath("//td//a[contains(text(),'SIGN-ON')]"));
		WebElement registerLink = driver.findElement(By.xpath("//td//a[contains(text(),'REGISTER')]"));
		WebElement supportLink = driver.findElement(By.xpath("//td//a[contains(text(),'SUPPORT')]"));
		WebElement contactLink = driver.findElement(By.xpath("//td//a[contains(text(),'CONTACT')]"));
		
		//Different Section locators
		WebElement featuredDestiSec = driver.findElement(By.xpath("//td//img[@alt='Featured Destination: Aruba']"));
		WebElement findFlightSec = driver.findElement(By.xpath("//td//img[@alt='Find a Flight']"));
		WebElement destinationsSec  = driver.findElement(By.xpath("//td//img[@alt='Desinations']"));
		WebElement vacationsSec = driver.findElement(By.xpath("//td//img[@alt='vacations']"));
		WebElement registerSec = driver.findElement(By.xpath("//td//img[@alt='Register']"));
		WebElement linksSec = driver.findElement(By.xpath("//td//img[@alt='Links']"));
		WebElement tourTips = driver.findElement(By.xpath("//td//img[@alt='Tour Tips']"));
		
		//Verify Home link is present or not
		if (homeLink.size() > 0)
			System.out.println("\nHome link is present in the left Menu");
		else
			System.out.println("Home link is not present in the left Menu");
		
		//Verify Flight link is present or not
		if (flightsLink.size() > 0)
			System.out.println("Flight link is present in the left Menu");
		else
			System.out.println("Flight link is not present in the left Menu");
		
		//Verify Hotel link is present or not
		if (hotelsLink.size() > 0)
			System.out.println("Hotels link is present in the left Menu");
		else
			System.out.println("Hotels link is not present in the left Menu");
		
		//Verfiy link is present or not with isDisplyaed() method
		if( singOnLink.isDisplayed()){
			System.out.println("SignOn button is Present");
			}
		else{
			System.out.println("SignOn button is not Present");
			}
		
		
		Assert.assertEquals(true, featuredDestiSec.isDisplayed());
		
		
		driver.close();
		
		}
		

		
	}


